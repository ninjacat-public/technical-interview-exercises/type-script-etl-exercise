export type OldScoringTable = {
  [key in number]: string[];
};

export type NewScoringTable = {
  [key in string]: number;
};

export function transform(oldTable: OldScoringTable): NewScoringTable {
  const newTable: NewScoringTable = {};

  Object.entries( oldTable ).forEach( ([score, letters]) => {
    letters.forEach( (letter) => {
      newTable[ letter.toLowerCase() ] = Number( score );
    });
  });
  return newTable;
}
