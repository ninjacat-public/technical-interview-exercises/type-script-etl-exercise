import { OldScoringTable, NewScoringTable, transform } from "../etl";

const OLD_SCORING_TABLE: OldScoringTable = {
  1: ["A", "E", "I", "O", "U", "L", "N", "R", "S", "T"],
  2: ["D", "G"],
  3: ["B", "C", "M", "P"],
  4: ["F", "H", "V", "W", "Y"],
  5: ["K"],
  8: ["J", "X"],
  10: ["Q", "Z"],
};

const NEW_SCORING_TABLE: NewScoringTable = {
  a: 1, b: 3, c: 3, d: 2, e: 1, f: 4, g: 2, h: 4, i: 1, j: 8, k: 5, l: 1, m: 3,
  n: 1, o: 1, p: 3, q: 10, r: 1, s: 1, t: 1, u: 1, v: 4, w: 4, x: 8, y: 4, z: 10
};

describe("transform", () => {
  it("transforms a scoring table in the old format into one in the new format", () => {
    expect( transform( OLD_SCORING_TABLE ) ).toStrictEqual( NEW_SCORING_TABLE );
  });

  it("throws if the old scoring table contain a duplicate letter", () => {
    const oldTableWithDuplicate = {...OLD_SCORING_TABLE, 11: ["A"]};
    expect( () => transform(oldTableWithDuplicate) ).toThrow();
  });
});